import pandas as pd
import nltk
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.feature_extraction.text import TfidfVectorizer
from imblearn.pipeline import make_pipeline
from sklearn.metrics import f1_score
from sklearn.model_selection import train_test_split
from imblearn.over_sampling import RandomOverSampler
from imblearn.under_sampling import RandomUnderSampler
from tqdm import tqdm
import pickle
import en_core_web_sm
nlp = en_core_web_sm.load()


def normalize(text):
    doc = nlp(text)
    lemma_list = [token.lemma_ for token in doc]
    filtered_sentence = list(filter(lambda w: not nlp.vocab[w].is_stop, lemma_list))
    lines = ' '.join(filtered_sentence)
    is_noun = lambda pos: pos[:2] == 'NN'
    tokenized = nltk.word_tokenize(lines)
    nouns = [word for (word, pos) in nltk.pos_tag(tokenized) if is_noun(pos)]
    return ' '.join(nouns)


data_train = pd.read_csv('data_train.csv')
data_test = pd.read_csv('data_test_all.csv')
submission = data_test['id'].copy().to_frame()
submission['tags'] = ''

if 'normalized' not in data_train.columns:
    tqdm.pandas()
    data_train['normalized'] = data_train['plot_synopsis'].progress_apply(normalize)
    data_train.to_csv('data_train.csv',columns=data_train.columns, index=False)


if 'normalized' not in data_test.columns:
    tqdm.pandas()
    data_test['normalized'] = data_test['plot_synopsis'].progress_apply(normalize)
    data_test.to_csv('data_test_all.csv',columns=data_test.columns, index=False)


data_train['splitted_tags'] = data_train.tags.str.split(', ')
mb = MultiLabelBinarizer()
mb.fit(data_train['splitted_tags'])
encoded_tags = mb.transform(data_train['splitted_tags'])

f1_train = 0
f1_valid = 0
genres = ['murder', 'romantic', 'comedy', 'fantasy', 'flashback']
X_test = data_test['normalized']
models = []

for i in range(encoded_tags.shape[1]):

    if mb.classes_[i] in genres:
        print(mb.classes_[i])

        X_train, X_valid, y_train, y_valid\
            = train_test_split(data_train['normalized'], encoded_tags[:, i], test_size=0.2, random_state=42,
                               stratify=encoded_tags[:, i])

        pipeline = make_pipeline(
            TfidfVectorizer(max_df=0.8, max_features=10000),
            RandomOverSampler(),
            RandomUnderSampler(),
            ExtraTreesClassifier(random_state=0, max_depth=3, n_estimators=1000)
        )

        pipeline.fit(X_train, y_train)
        models.append(pipeline)
        pred_train = pipeline.predict(X_train)
        pred_valid = pipeline.predict(X_valid)
        pred_test = pipeline.predict(X_test)
        print(pred_test)
        j = 0
        for pred in pred_test:
            sub = submission.iloc[j, submission.columns.get_loc('tags')]
            if pred == 1:
                if sub == '':
                    submission.iloc[j, submission.columns.get_loc('tags')] = mb.classes_[i]
                else:
                    submission.iloc[j, submission.columns.get_loc('tags')] = sub + ',' + mb.classes_[i]
            j += 1

        f1_train += f1_score(y_train, pred_train)
        f1_valid += f1_score(y_valid, pred_valid)

f1_train /= len(genres)
f1_valid /= len(genres)
print('Train F1 Score:')
print(f1_train)
print('Valid F1 Score:')
print(f1_valid)
submission.to_csv('submission.csv', index=False)
pickle.dump(models, open('models.sav', 'wb'))
